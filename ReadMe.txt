Install the service:
 - Run the Visual Studio Developer's command prompt as administrator.
 - Enter "Installutil [path of your Windows Service exe file]"

Configure the Service: 
 - Create User account for the service
 - In Windows Services, configure the service to use the new user account.
 - In Windows explorer, give new user read-only rights to RTF folder
 - In the service's app.config, set the folder names for the RTF folder and for the log folder.
 
Start the Service: 
 - From elevated cmd, enter "net start CTFileWatcher"

Log File Contents:
  - Date and time of whe the service was started or stopped.
  - Date, time, and file name of any file created changed or deleted.
  - Date, time, filename, and slope value or "noslope" from rtf files.
  - Date, time, and exception message of any exceptions that occur!
