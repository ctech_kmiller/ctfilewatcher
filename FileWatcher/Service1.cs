﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.Threading;

// ReSharper disable LocalizableElement

namespace FileWatcher
{
    public partial class Service1 : ServiceBase
    {
        #region Fields

        private readonly string _logFile = ConfigurationManager.AppSettings["LogPath"] + "\\CTFileWatcher.log";

        private readonly string _watchPath = ConfigurationManager.AppSettings["WatchPath"];

        #endregion

        #region Constructors

        public Service1()
        {
            InitializeComponent();
            fileSystemWatcherWatchTemp.Created += FileSystemWatcherWatchTemp_Created;
            //fileSystemWatcherWatchTemp.Created += (s, e) => { File.AppendAllText(_logFile, $"created\t{e.Name}\t{DateTime.Now}\r\n"); };
            fileSystemWatcherWatchTemp.Changed += (s, e) => { File.AppendAllText(_logFile, $"changed\t{e.Name}\t{DateTime.Now}\r\n"); };
            fileSystemWatcherWatchTemp.Deleted += (s, e) => { File.AppendAllText(_logFile, $"deleted\t{e.Name}\t{DateTime.Now}\r\n"); };
            fileSystemWatcherWatchTemp.Renamed += (s, e) => { File.AppendAllText(_logFile, $"renamed\t{e.Name} <- {e.OldName}\t{DateTime.Now}\r\n"); };
            fileSystemWatcherWatchTemp.Error += (s, e) => { File.AppendAllText(_logFile, $"deleted\t{e.GetException()}\t{DateTime.Now}\r\n"); };
        }

        private void FileSystemWatcherWatchTemp_Created(object sender, FileSystemEventArgs e)
        {
            try
            {
                File.AppendAllText(_logFile, $"created\t{e.Name}\t{DateTime.Now}\r\n");
                if (!e.Name.EndsWith(".rtf", StringComparison.CurrentCultureIgnoreCase)) return;
                var fileFullName = $"{_watchPath}\\{e.Name}";
                if (!SpinWait.SpinUntil(()=> !IsFileLocked(fileFullName), 1000)) throw  new Exception("File locked");
                var fileContents = File.ReadAllText(fileFullName);
                var match = Regex.Match(fileContents, @" PLXQ.*nm\\cell.*?\s*?.* ([+-]?[0-9]*[.]?[0-9]+)\\cell");
                if (match.Success)
                {
                    File.AppendAllText(_logFile, $"slope:{match.Groups[1].Value}\t{e.Name}\t{DateTime.Now}\r\n");
                }
                else
                {
                    File.AppendAllText(_logFile, $"noSlope\t{e.Name}\t{DateTime.Now}\r\n");
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(_logFile, $"exception\t{ex}\t{DateTime.Now}\r\n");
            }
        }

        #endregion

        #region Other Members

        protected override void OnStart(string[] args)
        {
            //Debugger.Launch();
            fileSystemWatcherWatchTemp.Path = _watchPath;
            File.AppendAllText(_logFile, $"started\t{DateTime.Now}\r\n");
        }

        protected override void OnStop()
        {
            //Create_ServiceStoptextfile();
            File.AppendAllText(_logFile, $"stopped\t{DateTime.Now}\r\n");
        }

        public bool IsFileLocked(string filePath)
        {
            try
            {
                using (File.Open(filePath, FileMode.Open)) { }
                return false;
            }
            catch (IOException e)
            {
                var errorCode = Marshal.GetHRForException(e) & ((1 << 16) - 1);
                return errorCode == 32 || errorCode == 33;
            }

        }

        #endregion
    }
}